-------------------------------------------------------------------------------
ProxyPay Eurbank module, with redirection, for Drupal 6.x
  by Xaris Tsimpouris, http://drupal.org/user/773446
  imported to D6 from Greek_bank_Eurobank, http://www.ubercart.org/contrib/13345
-------------------------------------------------------------------------------

DESCRIPTION:
This module makes the task of backing up your Drupal database and migrating data
from one Drupal install to another easier. It provides a function to backup the
entire database to file or download, and to restore from a previous backup. You
can also schedule the backup operation. Compression of backup files is also
supported.

There are options to exclude the data from certain tables (such as cache or
search index tables) to increase efficiency by ignoring data that does not need
to be backed up or migrated.

The backup files are a list of SQL statements which can be executed with a tool
such as phpMyAdmin or the command-line mysql client.

-------------------------------------------------------------------------------

INSTALLATION:
* Put the module in your drupal modules directory and enable it in 
  admin/build/modules. 
* Enable and configure eurobank payment method from here, admin/store/settings/payment/edit/methods

-------------------------------------------------------------------------------
